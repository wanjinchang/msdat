% Creation      : 15:49 17-Nov-2016 Thursday
% Last Revision : 16:18 18-Nov-2016 Thursday
% Author        : Xinyu Wang
% File Type     : matlab
%
% This script is a demo for Multi-Scale Domain Adaption Tracking (MSDAT)
% -------------------------------------------------------------------------
% Our Test environment is here:
% CPU    : Intel Xeon E3-1231 v3 @ 3.40GHz × 8
% GPU    : Nvidia Geforce GTX 1070/PCIe/SSE2
% Cuda   : version - 8.0
% Cudnn  : version - 5.0
% System : Ubuntu 14.04 LTS x64
% Matlab : R2015b
% Attention:
% To Run this demo, you need to compile caffe-master on your own device, and
% move the matcaffe (+caffe, caffe_.mexa64) to this direction. Besides
% these, you need to download our pretrained model and caffe prototxt to
% employee the feature extracter we used in this demo.
% After all preparations for envrionment configuration, you have to modify
% the OTB50 dataset path as your own dataset path in [utils/set_conf.m]
% file, and other defualt parameters (eg. gpu_enable, gpu_id etc.) are
% supposed to be modified to fit your own device too.

%% Intialize
clc;
clear;
close all;

%% Set default parameters
addpath('utility');
addpath('PATH_TO_CAFFE/caffe-master/matlab/'); % add your matcaffe path
tracking_conf = set_conf(); % general configureation for tracking

%% Tracking demo for one video
run_tracker('Bolt', tracking_conf);


