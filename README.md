# README #

### Overview ###

* This is the test code of the Multi-Scale Domain Adaptation Tracker (MSDAT)
* V0.1

### Install ###

* Install the Caffe deep learning toolbox with the MatCaffe interface
* In demo.m, replace the "PATH_TO_CAFFE" in "addpath('PATH_TO_CAFFE/caffe-master/matlab/')"
  with the appropreate path in your machine
* run the script demo.m and have fun

#### Authors ###
* Xinyu Wang, Jiangxi Normal University
* Hanxi Li, Jiangxi Normal University
* Yi Li, Toyota Research Institute of North America
* Fumin Shen, University of Electronic Science and Technology of China 
* Fatih Porikli, Australian National University, Data61