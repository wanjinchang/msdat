% Creation      : 16:16 17-Nov-2016 Thursday
% Last Revision : 16:16 17-Nov-2016 Thursday
% Author        : Xinyu Wang
% File Type     : matlab
%
% This function contains the whole pipline of Channel Reduction Visual
% Tracking.

function [positions, time] = tracker_ensemble(video_path, img_files, pos, target_sz, tracking_conf)
%% Set some default parameters

% Get image size and search window size
im_sz     = size(imread([video_path img_files{1}]));
window_sz = get_search_window(target_sz, im_sz, tracking_conf.padding);
cell_sz   = floor(min(tracking_conf.max_cell_sz, max(tracking_conf.min_cell_size, sqrt(target_sz(1) * target_sz(2)) / tracking_conf.cell_sz_ratio)));
% Compute the sigma for the Gaussian function label
output_sigma = sqrt(prod(target_sz)) * tracking_conf.output_sigma_factor / cell_sz;

%create regression labels, gaussian shaped, with a bandwidth
%proportional to target size    d=bsxfun(@times,c,[1 2]);

l1_patch_num = floor(window_sz/ cell_sz);

% Pre-compute the Fourier Transform of the Gaussian function label
yf = fft2(gaussian_shaped_labels(output_sigma, l1_patch_num));

% Pre-compute and cache the cosine window (for avoiding boundary discontinuity)
cos_window = hann(size(yf,1)) * hann(size(yf,2))';

% Create video interface for visualization
if tracking_conf.show_results
    update_visualization = show_video(img_files, video_path);
end

% Initialize variables for calculating FPS and distance precision
time      = 0;
positions = zeros(numel(img_files), 2);
nweights  = reshape(tracking_conf.nweights,1,1,[]);

% Note: variables ending with 'f' are in the Fourier domain.
model_xf     = cell(1, tracking_conf.layer_num);
model_alphaf = cell(1, tracking_conf.layer_num);

% init pos shift
pos_shift = 100;
zf = [];
kzf = [];

%% Tracking
for frame = 1:numel(img_files),
    im = imread([video_path img_files{frame}]); % Load the image at the current frame
    if ismatrix(im)
        im = cat(3, im, im, im);
    end
    tracking_time = tic();
    % ================================================================================
    % Predicting the object position from the learned object model
    % ================================================================================
    if frame > 1
        % Extracting hierarchical convolutional features
        feat = extractFeature(im, pos, window_sz, cos_window, tracking_conf);
        % Predict position
        last_pos = pos;
        [pos, zf]  = predictPosition(feat, pos, nweights, cell_sz, l1_patch_num, ...
            model_xf, model_alphaf, tracking_conf);
        pos_shift = pos - last_pos;
%       fprintf('%d\n',pos_shift);
    end
    
    % ================================================================================
    % Learning correlation filters over hierarchical convolutional features
    % ================================================================================
    if frame == 1 || sum(abs(pos_shift)) > 0
        feat  = extractFeature(im, pos, window_sz, cos_window, tracking_conf);
        zf = [];
    end
    % Model update
    [model_xf, model_alphaf] = updateModel(feat, yf, tracking_conf.interp_factor, tracking_conf.lambda, frame, ...
        model_xf, model_alphaf, zf);

    % ================================================================================
    % Save predicted position and timing
    % ================================================================================
    positions(frame,:) = pos;
    time = time + toc(tracking_time);
          
    
    % Visualization
    if tracking_conf.show_results
        box = [pos([2,1]) - target_sz([2,1])/2, target_sz([2,1])];
        stop = update_visualization(frame, box);
        if stop, break, end  %user pressed Esc, stop early
        drawnow;
        % pause(0.05)  % uncomment to run slower
    end
%     pause();
end

end

function [pos, zf_cp] = predictPosition(feat, pos, nweights, cell_size, l1_patch_num, ...
    model_xf, model_alphaf, tracking_conf)
% ================================================================================
% Compute correlation filter responses at each layer
% ================================================================================
res_layer = zeros([l1_patch_num, tracking_conf.layer_num]);
zf_s = cellfun(@(x) fft2(x), feat, 'UniformOutput', false);
zf_cp = cell(tracking_conf.layer_num, 1);
for ii = 1 : tracking_conf.layer_num
    zf = zf_s{ii};   
    zf_cp{ii} = zf;
    kzf=sum(zf .* conj(model_xf{ii}), 3) / numel(zf);   
    res_layer(:,:,ii) = real(fftshift(ifft2(model_alphaf{ii} .* kzf)));  %equation for fast detection
end

% Combine responses from multiple layers (see Eqn. 5)
response = sum(bsxfun(@times, res_layer, nweights), 3);

% ================================================================================
% Find target location
% ================================================================================
% Target location is at the maximum response. we must take into
% account the fact that, if the target doesn't move, the peak
% will appear at the top-left corner, not at the center (this is
% discussed in the KCF paper). The responses wrap around cyclically.
[vert_delta, horiz_delta] = find(response == max(response(:)), 1);
vert_delta  = vert_delta  - floor(size(zf,1)/2);
horiz_delta = horiz_delta - floor(size(zf,2)/2);

% Map the position to the image space
pos = pos + cell_size * [vert_delta - 1, horiz_delta - 1];
end


function [model_xf, model_alphaf] = updateModel(feat, yf, interp_factor, lambda, frame, ...
    model_xf, model_alphaf, zf)

numLayers = length(feat);

% ================================================================================
% Initialization
% ================================================================================
xf       = cell(1, numLayers);
alphaf   = cell(1, numLayers);

% ================================================================================
% Model update
% ================================================================================
if isempty(zf)
    for ii=1 : numLayers
        xf{ii} = fft2(feat{ii});
        kf = sum(xf{ii} .* conj(xf{ii}), 3) / numel(xf{ii});
        alphaf{ii} = yf./ (kf+ lambda);   % Fast training
    end
else
    xf = zf;
    for ii=1 : numLayers
        kf = sum(xf{ii} .* conj(xf{ii}), 3) / numel(xf{ii});
        alphaf{ii} = yf./ (kf + lambda);   % Fast training
    end
end

% Model initialization or update
if frame == 1,  % First frame, train with a single image
    for ii=1:numLayers
        model_alphaf{ii} = alphaf{ii};
        model_xf{ii} = xf{ii};
    end
else
    % Online model update using learning rate interp_factor
    for ii=1:numLayers
        model_alphaf{ii} = (1 - interp_factor) * model_alphaf{ii} + interp_factor * alphaf{ii};
        model_xf{ii}     = (1 - interp_factor) * model_xf{ii}     + interp_factor * xf{ii};
    end
end


end

function feat  = extractFeature(im, pos, window_sz, cos_window, tracking_conf)

% Get the search window from previous detection
patch = get_subwindow(im, pos, floor(window_sz * tracking_conf.window_sz_times));

% Extracting hierarchical convolutional features
feat  = get_features(patch, cos_window, tracking_conf);
end
