% Creation      : 15:53 17-Nov-2016 Thursday
% Last Revision : 15:15 21-Nov-2016 Monday
% Author        : Xinyu Wang
% File Type     : matlab
%
% This function set some default parameters, you are supposed to modify
% following default parameters to fit your own device:
% [OTB_path]  : This parameters is the path of OTB50 (Object Tracking
% Benchmark) on your own device. If you don't have this dataset, we also
% provide a demo video in the './data' directory
% [EnableGPU] : To enable caffe gpu mode
% [GPU_ID]    : If you have more than one gpu, you can select the gpu id here
% [net_path]  : the caffe prototxt file of convolutional neural network
% [pretrained model] : The model we pre-trained use {Vot2013, Vot2014,
% Vot2015} - {OTB100}

function [tracking_conf] = set_conf()
%% General Configurations
tracking_conf.OTB_path = './videos/'; % Set your OTB50 path here
tracking_conf.enableGPU = true;               % Enable GPU
tracking_conf.GPU_ID = 1;                     % Gpu id

%% Tracking Configurations
tracking_conf.padding = struct('generic', 1.8, 'large', 1, 'height', 0.4); % Extra area surrounding the target
tracking_conf.lambda = 1e-4;              % Regularization parameter 
tracking_conf.output_sigma_factor = 0.10; % Spatial bandwidth (proportional to the target size)
tracking_conf.interp_factor = 0.01;       % Model learning rate
tracking_conf.show_plots = false;         % Show plot
tracking_conf.show_results = false;       % Show results
tracking_conf.max_cell_sz = 20;           % Spatial cell size
tracking_conf.min_cell_size = 4;          % Spatial cell size
tracking_conf.cell_sz_ratio = 30;         % Spatial cell size
tracking_conf.window_sz_times = 1;        % KCF subwindow size

%% Feature Extracter Configurations
tracking_conf.net_path = './model/feature_exracter_vgg_19.prototxt';
tracking_conf.pretrained_model = './model/pretrained_model.caffemodel';
tracking_conf.nweights = [0.02, 0.4, 0.6];  % Weights for combining correlation filter responses, conv3_5, conv4_5, conv5_5
tracking_conf.layer_num = 3;
tracking_conf.net_input_sz = [224, 224];
% mean value of image in each channel [123.68, 116.779, 103.939]
tracking_conf.avg_img(:, :, 1) = ones(tracking_conf.net_input_sz) * 123.680;
tracking_conf.avg_img(:, :, 2) = ones(tracking_conf.net_input_sz) * 116.779;
tracking_conf.avg_img(:, :, 3) = ones(tracking_conf.net_input_sz) * 103.939;

end