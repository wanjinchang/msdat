% Creation      : 16:10 17-Nov-2016 Thursday
% Last Revision : 15:17 21-Nov-2016 Monday
% Author        : Xinyu Wang
% File Type     : matlab
%
% This function use our pre-trained model to extract channel reducted
% convolutional features for visual tracking

function feat = get_features(im, cos_window, tracking_conf)
%% Init caffe while runing this programe at the first time
global net
if isempty(net)
    start_init = tic();
    fprintf('First time runing, initializing caffe ...\n');
    if tracking_conf.enableGPU
        GPU_ID = tracking_conf.GPU_ID;
        caffe.set_device(GPU_ID-1);
        caffe.reset_all();
        caffe.set_mode_gpu();
    else
        caffe.reset_all();
        caffe.set_mode_cpu();
    end
    net = caffe.Net(tracking_conf.net_path,'test');
    net.copy_from(tracking_conf.pretrained_model);
    init_time = toc(start_init); % We will minus this init time when we caculate the FPS
    fprintf('Caffe Init Done! Cost %.2f (s)\n', init_time);
end

sz_window = size(cos_window);

%% Pre-processing the net-input data
% Because the caffe's blobs use [N K H W] order, and the images channels are
% ordered by [B G R], so we transfer the data to the caffe format
img = single(im);
img = imResample(img, tracking_conf.net_input_sz); % resize the img to CNN input size
img = img - tracking_conf.avg_img; % minus the average imgae
img = img(:, :, [3, 2, 1]); % transfer the channel from [R, G, B] to [B, G, R]
img = permute(img, [2, 1, 3]); % permute the image from [W, H, C] to [H, W, C]

% Forward propagation
if size(net.blobs('data').get_data(), 1) ~= size(img, 1)
    net.blobs('data').reshape([size(img, 1), size(img, 2), size(img, 3), size(img, 4)]);
end
feat = net.forward({img});

% Cause the width and height has been permute before forward propagation to
% fit caffe foramt, we need to permute the features back to original format
% that used in matlab
feat = cellfun(@(x) permute(x, [2, 1, 3]), feat, 'UniformOutput', false);
feat = cellfun(@(x) imResample(x, sz_window(1:2)), feat, 'UniformOutput', false);
feat = cellfun(@(x) bsxfun(@times, x, cos_window), feat, 'UniformOutput', false);

end