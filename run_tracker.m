% Creation      : 15:57 17-Nov-2016 Thursday
% Last Revision : 15:57 17-Nov-2016 Thursday
% Author        : Xinyu Wang
% File Type     : matlab


function [precision, fps] = run_tracker(video, tracking_conf)

% We were given the name of a single video to process.
% get image file names, initial state, and ground truth for evaluation
[img_files, pos, target_sz, ground_truth, video_path] = load_video_info(tracking_conf.OTB_path, video);
% Call tracker function with all the relevant parameters
[positions, time] = tracker_ensemble(video_path, img_files, pos, target_sz, tracking_conf);
% pause(1);
close;

% Calculate and show precision plot, as well as frames-per-second
precisions = precision_plot(positions, ground_truth, video, tracking_conf.show_plots);

fps = numel(img_files) / time;

fprintf('%12s - Precision (20px):% 1.3f, FPS:% 4.2f\n', video, precisions(20), fps)

if nargout > 0,
    %return precisions at a 20 pixels threshold
    precision = precisions(20);
end

end